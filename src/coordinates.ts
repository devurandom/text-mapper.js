// SPDX-License-Identifier: AGPL-3.0-or-later

export type Coordinates = { x: number; y: number };
export const add = <T extends Coordinates>(p1: T, p2: T): T => ({
    ...p1,
    x: p1.x + p2.x,
    y: p1.y + p2.y,
});
export const sub = <T extends Coordinates>(p1: T, p2: T): T => ({
    ...p1,
    x: p1.x - p2.x,
    y: p1.y - p2.y,
});
export const mul = <T extends Coordinates>(p: T, q: number): T => ({
    ...p,
    x: p.x * q,
    y: p.y * q,
});
export const interpolate = <T extends Coordinates>(
    p1: T,
    p2: T,
    qs: number[],
): T[] => {
    const delta = sub(p2, p1);
    return qs.map((q) => add(p1, mul(delta, q)));
};
export const distanceSquared = (p1: Coordinates, p2: Coordinates): number => {
    const diff = sub(p2, p1);
    return diff.x * diff.x + diff.y * diff.y;
};
export const epsilon = 0.5;

export type MapCoordinates = { type: "mapCoordinates" } & Coordinates;
export const asMapCoordinates = ({ x, y }: Coordinates): MapCoordinates => ({
    type: "mapCoordinates",
    x,
    y,
});
export const MapOrigin = asMapCoordinates({ x: 0, y: 0 });

export type ScreenCoordinates = { type: "screenCoordinates" } & Coordinates;
export const asScreenCoordinates = ({
    x,
    y,
}: Coordinates): ScreenCoordinates => ({
    type: "screenCoordinates",
    x,
    y,
});
export const ScreenOrigin = asScreenCoordinates({ x: 0, y: 0 });
