// SPDX-License-Identifier: AGPL-3.0-or-later

import { expect, test, describe } from "bun:test";
import { readFileSync } from "fs";
import { XMLParser } from "fast-xml-parser";
import {
    boundingBox,
    mapToScreen,
    numRowsByLevel,
    regionOffsetByLevel,
    render,
    renderClosedLoopPath,
    renderCoordinateNumber,
    renderLine,
    renderRegionLabel,
    renderRegularPath,
} from "./render.js";
import {
    asMapCoordinates,
    asScreenCoordinates,
    Coordinates,
    MapCoordinates,
} from "./coordinates.js";
import { closeLoop } from "./collection";
import { parseFile } from "./parser";

const expectCoordinatesToBeCloseTo = (
    actual: Coordinates,
    expected: Coordinates,
    numDigits?: number,
): void => {
    expect(actual.x).toBeCloseTo(expected.x, numDigits);
    expect(actual.y).toBeCloseTo(expected.y, numDigits);
};

describe("numRowsByLevel", () => {
    test("", () => {
        expect(
            numRowsByLevel(
                new Map([
                    [
                        0,
                        [
                            {
                                type: "region",
                                level: 0,
                                coordinates: asMapCoordinates({ x: 1, y: 3 }),
                                tags: [],
                                labels: [],
                            },
                            {
                                type: "region",
                                level: 0,
                                coordinates: asMapCoordinates({ x: 1, y: 4 }),
                                tags: [],
                                labels: [],
                            },
                        ],
                    ],
                    [
                        1,
                        [
                            {
                                type: "region",
                                level: 1,
                                coordinates: asMapCoordinates({ x: 1, y: 1 }),
                                tags: [],
                                labels: [],
                            },
                            {
                                type: "region",
                                level: 1,
                                coordinates: asMapCoordinates({ x: 1, y: 2 }),
                                tags: [],
                                labels: [],
                            },
                            {
                                type: "region",
                                level: 1,
                                coordinates: asMapCoordinates({ x: 1, y: 3 }),
                                tags: [],
                                labels: [],
                            },
                        ],
                    ],
                    [
                        2,
                        [
                            {
                                type: "region",
                                level: 2,
                                coordinates: asMapCoordinates({ x: 1, y: 1 }),
                                tags: [],
                                labels: [],
                            },
                            {
                                type: "region",
                                level: 2,
                                coordinates: asMapCoordinates({ x: 1, y: 2 }),
                                tags: [],
                                labels: [],
                            },
                        ],
                    ],
                    [
                        3,
                        [
                            {
                                type: "region",
                                level: 3,
                                coordinates: asMapCoordinates({ x: 1, y: 4 }),
                                tags: [],
                                labels: [],
                            },
                        ],
                    ],
                ]),
            ),
        ).toEqual(
            new Map([
                [0, 2],
                [1, 3],
                [2, 2],
                [3, 1],
            ]),
        );
    });
});

describe("regionOffsetByLevel", () => {
    test("multi-level", () => {
        expect(
            regionOffsetByLevel([
                {
                    type: "region",
                    level: 0,
                    coordinates: asMapCoordinates({ x: 1, y: 3 }),
                    tags: [],
                    labels: [],
                },
                {
                    type: "region",
                    level: 0,
                    coordinates: asMapCoordinates({ x: 1, y: 4 }),
                    tags: [],
                    labels: [],
                },
                {
                    type: "region",
                    level: 1,
                    coordinates: asMapCoordinates({ x: 1, y: 1 }),
                    tags: [],
                    labels: [],
                },
                {
                    type: "region",
                    level: 1,
                    coordinates: asMapCoordinates({ x: 1, y: 2 }),
                    tags: [],
                    labels: [],
                },
                {
                    type: "region",
                    level: 1,
                    coordinates: asMapCoordinates({ x: 1, y: 3 }),
                    tags: [],
                    labels: [],
                },
                {
                    type: "region",
                    level: 2,
                    coordinates: asMapCoordinates({ x: 1, y: 1 }),
                    tags: [],
                    labels: [],
                },
                {
                    type: "region",
                    level: 2,
                    coordinates: asMapCoordinates({ x: 1, y: 2 }),
                    tags: [],
                    labels: [],
                },
                {
                    type: "region",
                    level: 3,
                    coordinates: asMapCoordinates({ x: 1, y: 4 }),
                    tags: [],
                    labels: [],
                },
            ]),
        ).toEqual(
            new Map([
                [0, asMapCoordinates({ x: 0, y: 0 })],
                [1, asMapCoordinates({ x: 0, y: 5 })],
                [2, asMapCoordinates({ x: 0, y: 9 })],
                [3, asMapCoordinates({ x: 0, y: 12 })],
            ]),
        );
    });
});

describe("boundingBox", () => {
    test("single-region", () => {
        expect(
            boundingBox([
                {
                    type: "region",
                    level: 0,
                    coordinates: asMapCoordinates({ x: 1, y: 1 }),
                    tags: [],
                    labels: [],
                },
            ]),
        ).toEqual({ minX: 1, minY: 1, maxX: 1, maxY: 2 });
    });
    test("single-level", () => {
        expect(
            boundingBox([
                {
                    type: "region",
                    level: 0,
                    coordinates: asMapCoordinates({ x: 2, y: 2 }),
                    tags: [],
                    labels: [],
                },
                {
                    type: "region",
                    level: 0,
                    coordinates: asMapCoordinates({ x: 3, y: 3 }),
                    tags: [],
                    labels: [],
                },
            ]),
        ).toEqual({ minX: 2, minY: 2, maxX: 3, maxY: 4 });
    });
    test("multi-level", () => {
        expect(
            boundingBox([
                {
                    type: "region",
                    level: 0,
                    coordinates: asMapCoordinates({ x: 1, y: 1 }),
                    tags: [],
                    labels: [],
                },
                {
                    type: "region",
                    level: 1,
                    coordinates: asMapCoordinates({ x: 1, y: 1 }),
                    tags: [],
                    labels: [],
                },
            ]),
        ).toEqual({ minX: 1, minY: 1, maxX: 1, maxY: 4 });
    });
});

describe("mapToScreen", () => {
    [
        { input: { x: 0, y: 5 }, expected: { x: 0.0, y: 866.0 } },
        { input: { x: 1, y: 6 }, expected: { x: 150.0, y: 952.6 } },
        { input: { x: 2, y: 6 }, expected: { x: 300.0, y: 1039.2 } },
        { input: { x: 3, y: 6 }, expected: { x: 450.0, y: 952.6 } },
        { input: { x: 4, y: 6 }, expected: { x: 600.0, y: 1039.2 } },
        { input: { x: 5, y: 6 }, expected: { x: 750.0, y: 952.6 } },
    ]
        .map(({ input, expected }) => ({
            m: asMapCoordinates(input),
            s: asScreenCoordinates(expected),
        }))
        .forEach(({ m, s }) => {
            test(`0005-0506 trail: ${m.x},${m.y}=>${s.x},${s.y}`, () => {
                expectCoordinatesToBeCloseTo(mapToScreen(m), s, 1);
            });
        });
});

describe("renderCoordinateNumber", () => {
    [
        { input: 0, expected: "00" },
        { input: -1, expected: "-01" },
        { input: 1, expected: "01" },
        { input: 100, expected: "100" },
        { input: -100, expected: "-100" },
    ].forEach(({ input, expected }) => {
        test(`${input}=>${expected}`, () => {
            expect(renderCoordinateNumber(input)).toBe(expected);
        });
    });
});

describe("renderRegularPath", () => {
    test("0005-0506 trail", () => {
        expect(
            renderRegularPath(
                [
                    { x: 0, y: 5 },
                    { x: 1, y: 6 },
                    { x: 2, y: 6 },
                    { x: 3, y: 6 },
                    { x: 4, y: 6 },
                    { x: 5, y: 6 },
                ]
                    .map(asMapCoordinates)
                    .map(mapToScreen),
            ),
        ).toBe(
            // As rendered by Alex Schroeder's TextMapper:
            "M45.0,892.0 C75.0,909.3 45.0,892.0 75.0,909.3 S195.0,978.6 225.0,995.9 S345.0,1013.2 375.0,995.9 S495.0,978.6 525.0,995.9 S645.0,1013.2 675.0,995.9 L705.0,978.6",
        );
    });
});

describe("renderClosedLoopPath", () => {
    test("0205-0203-0103-0105-0205 trail", () => {
        expect(
            renderClosedLoopPath(
                closeLoop([
                    { x: 2, y: 5 },
                    { x: 2, y: 4 },
                    { x: 2, y: 3 },
                    { x: 1, y: 3 },
                    { x: 1, y: 4 },
                    { x: 1, y: 5 },
                    { x: 2, y: 5 },
                ])
                    .map(asMapCoordinates)
                    .map(mapToScreen),
            ),
        ).toBe(
            // As rendered by Alex Schroeder's TextMapper:
            "M225.0,822.7 C255.0,840.0 300.0,814.1 300.0,779.4 S300.0,640.9 300.0,606.2 S255.0,493.6 225.0,476.3 S150.0,485.0 150.0,519.6 S150.0,658.2 150.0,692.8 S195.0,805.4 225.0,822.7",
        );
    });
});

describe("renderLine", () => {
    test("0005-0506 trail", () => {
        expect(
            renderLine(
                {
                    trail: {
                        stroke: "#e3bea3",
                        "stroke-width": "6",
                        fill: "none",
                    },
                },
                new Map<number, MapCoordinates>([
                    [0, asMapCoordinates({ x: 0, y: 0 })],
                ]),
                {
                    type: "line",
                    id: "line0",
                    tag: "trail",
                    level: 0,
                    points: [
                        { x: 0, y: 5 },
                        { x: 5, y: 6 },
                    ].map(asMapCoordinates),
                },
            ),
        ).toBe(
            // As rendered by Alex Schroeder's TextMapper:
            '<path id="line0" stroke="#e3bea3" stroke-width="6" fill="none" d="M45.0,892.0 C75.0,909.3 45.0,892.0 75.0,909.3 S195.0,978.6 225.0,995.9 S345.0,1013.2 375.0,995.9 S495.0,978.6 525.0,995.9 S645.0,1013.2 675.0,995.9 L705.0,978.6" />',
        );
    });
    test("0205-0203-0103-0105-0205 trail", () => {
        expect(
            renderLine(
                {
                    trail: {
                        stroke: "#e3bea3",
                        "stroke-width": "6",
                        fill: "none",
                    },
                },
                new Map<number, MapCoordinates>([
                    [0, asMapCoordinates({ x: 0, y: 0 })],
                ]),
                {
                    type: "line",
                    id: "line0",
                    tag: "trail",
                    level: 0,
                    points: [
                        { x: 2, y: 5 },
                        { x: 2, y: 3 },
                        { x: 1, y: 3 },
                        { x: 1, y: 5 },
                        { x: 2, y: 5 },
                    ].map(asMapCoordinates),
                },
            ),
        ).toBe(
            // As rendered by Alex Schroeder's TextMapper:
            '<path id="line0" stroke="#e3bea3" stroke-width="6" fill="none" d="M225.0,822.7 C255.0,840.0 300.0,814.1 300.0,779.4 S300.0,640.9 300.0,606.2 S255.0,493.6 225.0,476.3 S150.0,485.0 150.0,519.6 S150.0,658.2 150.0,692.8 S195.0,805.4 225.0,822.7" />',
        );
    });
    test("0707-0607 communication", () => {
        expect(
            renderLine(
                {
                    communication: {
                        stroke: "#ff6347",
                        "stroke-width": "10pt",
                        fill: "none",
                        opacity: "0.7",
                    },
                },
                new Map<number, MapCoordinates>([
                    [0, asMapCoordinates({ x: 0, y: 0 })],
                ]),
                {
                    type: "line",
                    id: "line12",
                    tag: "communication",
                    level: 0,
                    points: [
                        { x: 7, y: 7 },
                        { x: 6, y: 7 },
                    ].map(asMapCoordinates),
                },
            ),
        ).toBe(
            // As rendered by Alex Schroeder's TextMapper:
            '<path id="line12" stroke="#ff6347" stroke-width="10pt" fill="none" opacity="0.7" d="M1005.0,1151.8 C975.0,1169.1 1005.0,1151.8 975.0,1169.1 L945.0,1186.5" />',
        );
    });
    test("0804-0605 communication", () => {
        expect(
            renderLine(
                {
                    communication: {
                        stroke: "#ff6347",
                        "stroke-width": "10pt",
                        fill: "none",
                        opacity: "0.7",
                    },
                },
                new Map<number, MapCoordinates>([
                    [0, asMapCoordinates({ x: 0, y: 0 })],
                ]),
                {
                    type: "line",
                    id: "line13",
                    tag: "communication",
                    level: 0,
                    points: [
                        { x: 8, y: 4 },
                        { x: 7, y: 5 },
                        { x: 6, y: 5 },
                    ].map(asMapCoordinates),
                },
            ),
        ).toBe(
            // As rendered by Alex Schroeder's TextMapper:
            '<path id="line13" stroke="#ff6347" stroke-width="10pt" fill="none" opacity="0.7" d="M1155.0,718.8 C1125.0,736.1 1155.0,718.8 1125.0,736.1 S1005.0,805.4 975.0,822.7 L945.0,840.0" />',
        );
    });
});

describe("renderRegionLabel", () => {
    test("without URL", () => {
        expect(
            renderRegionLabel(
                undefined,
                {
                    "font-size": "20pt",
                    dy: "5px",
                },
                {
                    "font-size": "20pt",
                    dy: "5px",
                    stroke: "white",
                    "stroke-width": "5pt",
                },
                asScreenCoordinates({ x: 150, y: 155.9 }),
                {
                    type: "regionLabel",
                    text: "tree",
                },
            ),
        ).toBe(
            // As rendered by Alex Schroeder's TextMapper:
            '<g><text text-anchor="middle" x="150.0" y="155.9" font-size="20pt" dy="5px" stroke="white" stroke-width="5pt">tree</text><text text-anchor="middle" x="150.0" y="155.9" font-size="20pt" dy="5px">tree</text></g>',
        );
    });
    test("with absolute URL", () => {
        expect(
            renderRegionLabel(
                "http://example.com/page/%s",
                {
                    "font-size": "20pt",
                    dy: "5px",
                },
                {
                    "font-size": "20pt",
                    dy: "5px",
                    stroke: "white",
                    "stroke-width": "5pt",
                },
                asScreenCoordinates({ x: 150, y: 155.9 }),
                {
                    type: "regionLabel",
                    text: "tree",
                },
            ),
        ).toBe(
            // As rendered by Alex Schroeder's TextMapper:
            '<g><text text-anchor="middle" x="150.0" y="155.9" font-size="20pt" dy="5px" stroke="white" stroke-width="5pt">tree</text><a xlink:href="http://example.com/page/tree"><text text-anchor="middle" x="150.0" y="155.9" font-size="20pt" dy="5px">tree</text></a></g>',
        );
    });
    test("with site-relative URL", () => {
        expect(
            renderRegionLabel(
                "/page/%s",
                {
                    "font-size": "20pt",
                    dy: "5px",
                },
                {
                    "font-size": "20pt",
                    dy: "5px",
                    stroke: "white",
                    "stroke-width": "5pt",
                },
                asScreenCoordinates({ x: 150, y: 155.9 }),
                {
                    type: "regionLabel",
                    text: "tree",
                },
            ),
        ).toBe(
            // As rendered by Alex Schroeder's TextMapper:
            '<g><text text-anchor="middle" x="150.0" y="155.9" font-size="20pt" dy="5px" stroke="white" stroke-width="5pt">tree</text><a xlink:href="/page/tree"><text text-anchor="middle" x="150.0" y="155.9" font-size="20pt" dy="5px">tree</text></a></g>',
        );
    });
    test("with relative URL", () => {
        expect(
            renderRegionLabel(
                "../%s",
                {
                    "font-size": "20pt",
                    dy: "5px",
                },
                {
                    "font-size": "20pt",
                    dy: "5px",
                    stroke: "white",
                    "stroke-width": "5pt",
                },
                asScreenCoordinates({ x: 150, y: 155.9 }),
                {
                    type: "regionLabel",
                    text: "tree",
                },
            ),
        ).toBe(
            // As rendered by Alex Schroeder's TextMapper:
            '<g><text text-anchor="middle" x="150.0" y="155.9" font-size="20pt" dy="5px" stroke="white" stroke-width="5pt">tree</text><a xlink:href="../tree"><text text-anchor="middle" x="150.0" y="155.9" font-size="20pt" dy="5px">tree</text></a></g>',
        );
    });
});

const parseXML = (input: string): unknown => {
    const xmlParser = new XMLParser({
        preserveOrder: true,
        ignoreAttributes: false,
        attributeNamePrefix: "@",
    });
    return xmlParser.parse(input);
};

describe("render", () => {
    test("gnomeyland", () => {
        const map = parseFile("examples/gnomeyland-example.txt", ["resources"]);
        expect(render(map)).toMatchSnapshot();
    });
    test("traveller", () => {
        const map = parseFile("examples/traveller-example.txt", ["resources"]);
        expect(render(map)).toMatchSnapshot();
    });
});

describe("our-vs-original", () => {
    test("gnomeyland", () => {
        const map = parseFile("examples/gnomeyland-example.txt", ["resources"]);
        const ourSVG = parseXML(render(map));
        const originalSVG = parseXML(
            readFileSync("examples/gnomeyland-example.svg", "utf-8"),
        );
        expect(ourSVG).toEqual(originalSVG);
    });
    test("traveller", () => {
        const map = parseFile("examples/traveller-example.txt", ["resources"]);
        const ourSVG = parseXML(render(map));
        const originalSVG = parseXML(
            readFileSync("examples/traveller-example.svg", "utf-8"),
        );
        //expect(ourSVG).toEqual(originalSVG);
        expect(ourSVG).toEqual(originalSVG);
    });
});
