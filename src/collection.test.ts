// SPDX-License-Identifier: AGPL-3.0-or-later

import { describe, expect, test } from "bun:test";
import {
    closeLoop,
    mergeObjects,
    objectGroupByKey,
    range,
    some,
    windows,
} from "./collection.js";

describe("objectGroupByKey", () => {
    test("empty", () => {
        expect(objectGroupByKey([], "type")).toEqual({});
    });
    test("group", () => {
        expect(
            objectGroupByKey(
                [
                    { type: "a", a: 1 },
                    { type: "b", b: 2 },
                ],
                "type",
            ),
        ).toEqual({
            a: [{ type: "a", a: 1 }],
            b: [{ type: "b", b: 2 }],
        });
    });
});

describe("mergeObjects", () => {
    test("empty", () => {
        expect(mergeObjects([])).toEqual({} as never);
    });
    test("non-overlapping", () => {
        expect(mergeObjects([{ a: 1 }, { b: 2 }])).toEqual({
            a: 1,
            b: 2,
        } as never); // TODO: How to teach TypeScript that we return not `T[]` but `T & T & ...` over all elements of `T`?
    });
    test("overlapping", () => {
        expect(mergeObjects([{ a: 1 }, { a: 2 }])).toEqual({ a: 2 });
    });
});

describe("some", () => {
    const pred = (x: string): string | null => (x === "true" ? "hello" : null);
    test("empty", () => {
        expect(some(pred, [])).toEqual(undefined as never); // TODO: TypeScript somehow misses that `some` can always return `undefined`.
    });
    test("no result", () => {
        expect(some(pred, ["false"])).toEqual(undefined as never); // TODO: TypeScript somehow misses that `some` can always return `undefined`.
    });
    test("first is result", () => {
        expect(some(pred, ["true", "false"])).toEqual("hello");
    });
    test("last is result", () => {
        expect(
            some((x) => (x === "true" ? "hello" : false), ["false", "true"]),
        ).toEqual("hello");
    });
});

describe("range", () => {
    test("0-0", () => {
        expect(range(0)).toEqual([]);
    });
    test("0-5", () => {
        expect(range(6)).toEqual([0, 1, 2, 3, 4, 5]);
    });
});

describe("windows", () => {
    test("0005-0506 trail", () => {
        expect(
            windows(2, [
                { x: 0, y: 5 },
                { x: 1, y: 6 },
                { x: 2, y: 6 },
                { x: 3, y: 6 },
                { x: 4, y: 6 },
                { x: 5, y: 6 },
            ]),
        ).toEqual([
            [
                { x: 0, y: 5 },
                { x: 1, y: 6 },
            ],
            [
                { x: 1, y: 6 },
                { x: 2, y: 6 },
            ],
            [
                { x: 2, y: 6 },
                { x: 3, y: 6 },
            ],
            [
                { x: 3, y: 6 },
                { x: 4, y: 6 },
            ],
            [
                { x: 4, y: 6 },
                { x: 5, y: 6 },
            ],
        ]);
    });
});

describe("closeLoop", () => {
    test("0205-0103-0202-0205 trail", () => {
        expect(
            closeLoop([
                { x: 2, y: 5 },
                { x: 2, y: 4 },
                { x: 2, y: 3 },
                { x: 1, y: 3 },
                { x: 1, y: 4 },
                { x: 1, y: 5 },
                { x: 2, y: 5 },
            ]),
        ).toEqual([
            { x: 1, y: 5 },
            { x: 2, y: 5 },
            { x: 2, y: 4 },
            { x: 2, y: 3 },
            { x: 1, y: 3 },
            { x: 1, y: 4 },
            { x: 1, y: 5 },
            { x: 2, y: 5 },
        ]);
    });
});
