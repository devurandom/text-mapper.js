// SPDX-License-Identifier: AGPL-3.0-or-later

import { describe, expect, test } from "bun:test";
import { interpolate } from "./coordinates";

describe("interpolate", () => {
    test("no qs", () => {
        expect(interpolate({ x: 0, y: 0 }, { x: 2, y: 2 }, [])).toEqual([]);
    });
    test("two points", () => {
        expect(interpolate({ x: 0, y: 0 }, { x: 2, y: 2 }, [0.5, 2.0])).toEqual(
            [
                { x: 1, y: 1 },
                { x: 4, y: 4 },
            ],
        );
    });
});
