// SPDX-License-Identifier: AGPL-3.0-or-later

export type WithRequired<T, K extends keyof T> = T & Required<Pick<T, K>>;

// Map.groupBy is not yet supported by many browsers,
// cf. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/groupBy
export const mapGroupBy = <T, K>(
    values: T[],
    callbackFn: (t: T) => K,
): Map<K, T[]> =>
    values.reduce((r, v) => {
        const k = callbackFn(v);
        return r.set(k, [...(r.get(k) ?? []), v]);
    }, new Map<K, T[]>());

// Object.groupBy is not yet supported by many browsers,
// cf. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/groupBy
export type GroupByKey<T extends { [k in K]: string }, K extends string> = {
    [t in T as t[K]]?: t[];
};
export const objectGroupByKey = <
    T extends { [k in K]: string },
    K extends string,
>(
    values: T[],
    key: K,
): GroupByKey<T, K> =>
    Object.fromEntries(
        values.reduce((r, t) => {
            const v = t[key];
            return r.set(v, [...(r.get(v) ?? []), t]);
        }, new Map<T[K], T[]>()),
    ) as GroupByKey<T, K>;

export const mergeObjects = <T>(coll?: T[]): T =>
    Object.assign({}, ...(coll ?? [])) as T;

export const some = <T, V>(pred: (t: T) => V, coll: T[]): V | undefined => {
    for (const x of coll) {
        const y = pred(x);
        if (y) {
            return y;
        }
    }
    return undefined;
};
export const range = (end: number, start = 0): number[] =>
    Array.from(Array(end).keys()).slice(start, end);
export const keys = <K, V>(m: Map<K, V>): K[] => Array.from(m.keys());
export const vals = <K, V>(m: Map<K, V>): V[] => Array.from(m.values());
export const zipmap = <K, V>(keys: K[], vals: V[]): Map<K, V> =>
    keys.reduce((m, k, i) => m.set(k, vals[i]), new Map<K, V>());
export const windows = <T, N extends number>(size: N, coll: T[]): T[][] =>
    Array.from({ length: coll.length - (size - 1) }, (_, i) =>
        coll.slice(i, i + size),
    );
export const closeLoop = <T>(coll: T[]): T[] => [
    coll[coll.length - 2],
    ...coll,
];
