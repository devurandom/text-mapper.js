// SPDX-License-Identifier: AGPL-3.0-or-later

import { writeFileSync } from "fs";
import {
    closeLoop,
    GroupByKey,
    keys,
    mapGroupBy,
    mergeObjects,
    objectGroupByKey,
    vals,
    windows,
    WithRequired,
    zipmap,
} from "./collection";
import {
    add,
    asMapCoordinates,
    asScreenCoordinates,
    Coordinates,
    distanceSquared,
    epsilon,
    interpolate,
    MapCoordinates,
    MapOrigin,
    ScreenCoordinates,
} from "./coordinates";
import {
    Attributes,
    Line,
    Region,
    RegionLabel,
    TextMapperValue,
} from "./parser";
import { fillIntermediatePoints } from "./path";

const { min, max } = Math;

type TextMapperMap = Required<
    Omit<
        GroupByKey<TextMapperValue, "type">,
        // Not important to the renderer:
        | "comment"
        | "include"
        // Replace with different type:
        | "baseURL"
        | "license"
        | "line"
        // Replace with different type under new name:
        | "baseAttributes"
        | "pathAttributes"
        | "textAttributes"
        | "labelAttributes"
        | "glowAttributes"
    >
> & {
    baseURL?: string;
    license: string;
    line: LineWithID[];
    baseAttrByTag: { [tag: string]: Attributes | undefined };
    pathAttrByTag: { [tag: string]: Attributes | undefined };
    textAttr: Attributes;
    labelAttr: Attributes;
    glowAttr: Attributes;
};
type LineWithID = Line & { id: string };
type LineWithLabel = WithRequired<Line, "label">;
type LineWithIDAndLabel = WithRequired<LineWithID, "label">;
const isLineWithIDAndLabel = (l: LineWithID): l is LineWithIDAndLabel =>
    l.label !== undefined;

export const numRowsByLevel = (
    regionsByLevel: Map<number, Region[]>,
): Map<number, number> => {
    const levels = keys(regionsByLevel);
    return zipmap(
        levels,
        levels.map((level) => {
            const ysAtLevel =
                regionsByLevel.get(level)?.map(({ coordinates: { y } }) => y) ??
                [];
            const minY = min(...ysAtLevel);
            const maxY = max(...ysAtLevel);
            return 1 + maxY - minY;
        }),
    );
};
export const regionOffsetByLevel = (
    regions: Region[],
): Map<number, MapCoordinates> => {
    const regionsByLevel = mapGroupBy(regions, ({ level }) => level);
    const ysAtGroundLevel =
        regionsByLevel.get(0)?.map(({ coordinates: { y } }) => y) ?? [];
    const minY = min(...ysAtGroundLevel);
    const rowsByLevel = numRowsByLevel(regionsByLevel);
    return zipmap(
        keys(rowsByLevel),
        vals(rowsByLevel)
            .reduce(
                ({ offsets, startOffset }, numRowsAtLevel) => {
                    // Plus the number of rows for every level:
                    const currentOffset = startOffset + numRowsAtLevel;
                    return {
                        offsets: [...offsets, currentOffset],
                        startOffset: currentOffset + 1, // Plus a separator row for every extra level:
                    };
                },
                {
                    offsets: [0],
                    // Start at first row of first level:
                    startOffset: minY,
                },
            )
            .offsets.map((yOffset) => asMapCoordinates({ x: 0, y: yOffset })),
    );
};

// Bounding box in abstract map coordinates (the same unit as Region["coordinates"]):
type BoundingBox = { minX: number; maxX: number; minY: number; maxY: number };
export const boundingBox = (regions: Region[]): BoundingBox => {
    const xs = regions.map(({ coordinates: { x } }) => x);
    const minX = min(...xs),
        maxX = max(...xs);

    const regionsByLevel = mapGroupBy(regions, ({ level }) => level);
    const levels = keys(regionsByLevel);
    const maxLevel = max(...levels); // 0-based!
    const rowsByLevel = numRowsByLevel(regionsByLevel);
    const totalNumRows = vals(rowsByLevel).reduce(
        (totalNumRows, numRows) => totalNumRows + numRows,
        0,
    );
    const ysAtGroundLevel =
        regionsByLevel.get(0)?.map(({ coordinates: { y } }) => y) ?? [];
    const minY = min(...ysAtGroundLevel),
        // This is not actually maxY, but maxY+1:
        maxY =
            // First row of the first level:
            minY +
            // Plus a separator row for every extra level:
            maxLevel +
            // Plus the number of rows for every level:
            totalNumRows;
    return { minX, maxX, minY, maxY };
};

const pr = ({ x, y }: Coordinates): string => `${x.toFixed(1)},${y.toFixed(1)}`;

const dx = 100;
const dy = 100 * Math.sqrt(3);
const hexPolygonPoints: ScreenCoordinates[] = [
    { x: -dx, y: 0 },
    { x: -dx / 2, y: dy / 2 },
    { x: dx / 2, y: dy / 2 },
    { x: dx, y: 0 },
    { x: dx / 2, y: -dy / 2 },
    { x: -dx / 2, y: -dy / 2 },
].map(asScreenCoordinates);

export const mapToScreen = ({ x, y }: MapCoordinates): ScreenCoordinates =>
    asScreenCoordinates({
        x: (x * dx * 3) / 2,
        y: y * dy - ((x % 2) * dy) / 2,
    });

// Bounding box in screen coordinates:
type ViewBox = {
    originX: number;
    originY: number;
    width: number;
    height: number;
};
const viewBox = ({ minX, minY, maxX, maxY }: BoundingBox): ViewBox => {
    const vx1 = Math.trunc((minX * dx * 3) / 2 - dx - 60),
        vy1 = Math.trunc((minY - 1.5) * dy),
        vx2 = Math.trunc((maxX * dx * 3) / 2 + dx + 60),
        vy2 = Math.trunc((maxY + 1) * dy);
    return { originX: vx1, originY: vy1, width: vx2 - vx1, height: vy2 - vy1 };
};
const renderViewBox = (regions: Region[]): string => {
    const bb = boundingBox(regions);
    const { originX, originY, width, height } = viewBox(bb);
    if (regions.length > 0) {
        return `viewBox="${originX.toFixed(0)} ${originY.toFixed(
            0,
        )} ${width.toFixed(0)} ${height.toFixed(0)}"`;
    }
    return "";
};

export const renderCoordinateNumber = (n: number): string => {
    const str = n.toString();
    const sign = str[0] === "-" ? "-" : "";
    const absolute = str[0] === "-" ? str.slice(1) : str;
    return absolute.length < 2 ? `${sign}0${absolute}` : `${sign}${absolute}`;
};

const renderAttributes = (a: Attributes): string =>
    Object.entries(a)
        .map(([k, v]) => `${k}="${v}"`)
        .join(" ");

const renderTagDef = (
    tag: string,
    path: string,
    pathAttributes: Attributes,
    glowAttributes: Attributes,
    baseAttributes?: Attributes,
): string => {
    const glowPath =
        path?.length > 0 &&
        (baseAttributes === undefined ||
            Object.entries(baseAttributes).length === 0)
            ? `<path ${renderAttributes(glowAttributes)} d="${path}" />`
            : "";
    const regularPath =
        path?.length > 0
            ? `<path ${renderAttributes(pathAttributes)} d="${path}" />`
            : "";
    const shape =
        baseAttributes !== undefined &&
        Object.entries(baseAttributes).length > 0
            ? `<polygon ${renderAttributes(
                  baseAttributes,
              )} points="${hexPolygonPoints.map(pr).join(" ")}" />`
            : "";
    return `<g id="${tag}">${glowPath}${shape}${regularPath}</g>`;
};

const renderDefs = ({
    region,
    line,
    path,
    svgRawDefinition,
    glowAttr,
    pathAttrByTag,
    baseAttrByTag,
}: TextMapperMap): string => {
    const defaultPathAttr = pathAttrByTag["default"];
    const regionTags = region?.flatMap(({ tags }) => tags) ?? [];
    const lineTags = line?.map(({ tag }) => tag) ?? [];
    const pathsByTag = objectGroupByKey(path ?? [], "tag");
    const rawDefs = svgRawDefinition?.map((r) => r.value) ?? [];
    const tagDefs = [...new Set([...regionTags, ...lineTags])]
        .sort()
        .map((tag) => ({
            tag,
            path: mergeObjects(pathsByTag[tag])?.path ?? "",
            baseAttr: baseAttrByTag[tag] ?? {},
            pathAttr: pathAttrByTag[tag] ?? {},
        }))
        .filter(
            ({ path, baseAttr }) =>
                path.length > 0 || Object.entries(baseAttr).length > 0,
        )
        .map(({ tag, path, baseAttr, pathAttr }) =>
            renderTagDef(
                tag,
                path,
                {
                    ...defaultPathAttr,
                    ...pathAttr,
                },
                glowAttr,
                baseAttr,
            ),
        );
    return `<defs>${[...rawDefs, ...tagDefs].join("\n")}</defs>`;
};

const offsetCoordinate = (
    offsetByLevel: Map<number, MapCoordinates>,
    level: number,
    p: MapCoordinates,
): MapCoordinates => add(p, offsetByLevel.get(level) ?? MapOrigin);
const offsetLine = (
    offsetByLevel: Map<number, MapCoordinates>,
    { level, points }: Line,
): MapCoordinates[] =>
    points.map((p) => offsetCoordinate(offsetByLevel, level, p));
const offsetRegion = (
    offsetByLevel: Map<number, MapCoordinates>,
    { level, coordinates }: Region,
): MapCoordinates => offsetCoordinate(offsetByLevel, level, coordinates);

const use = (tag: string, { x, y }: ScreenCoordinates): string =>
    `<use x="${x.toFixed(1)}" y="${y.toFixed(1)}" xlink:href="#${tag}" />`;

// Alex Schroeder's TextMapper calls this a "thing", even though it is always a region.
const renderRegionAsThing = (
    offsetByLevel: Map<number, MapCoordinates>,
    r: Region,
): string => {
    const screenCoordinates = mapToScreen(offsetRegion(offsetByLevel, r));
    return r.tags.map((t) => use(t, screenCoordinates)).join("\n");
};
const renderBackgrounds = (
    { region, baseAttrByTag }: TextMapperMap,
    offsetByLevel: Map<number, MapCoordinates>,
): string =>
    `<g id="backgrounds">${region
        // Keep only those tags that have attributes defined:
        .map(
            (r): Region => ({
                ...r,
                tags: r.tags.filter((t) => baseAttrByTag[t] !== undefined),
            }),
        )
        // Keep only those regions that have attributes defined for (some of) their tags,
        // assuming that those will also have a `<def/>` we can `href=...`:
        .filter(({ tags }) => tags.length > 0)
        .map((r) => renderRegionAsThing(offsetByLevel, r))
        .join("\n")}</g>`;

const pathForPair = (p1: Coordinates, p2: Coordinates): string => {
    const [p1Plus30pct, p1Plus50pct] = interpolate(p1, p2, [0.3, 0.5]);
    return `S${pr(p1Plus30pct)} ${pr(p1Plus50pct)}`;
};
const interpolatePair = (
    [p1, p2]: Coordinates[],
    qs: number[],
): Coordinates[] => interpolate(p1, p2, qs);
export const renderClosedLoopPath = (points: ScreenCoordinates[]): string => {
    const pointPairs = windows(2, points);
    const [secondToLastPlus50pct, secondToLastPlus70pct] = interpolatePair(
        pointPairs[0],
        [0.5, 0.7],
    );
    const [firstPlus30pct, firstPlus50pct] = interpolatePair(
        pointPairs[1],
        [0.3, 0.5],
    );
    const pathSegments = pointPairs
        .slice(2)
        .map(([p1, p2]) => pathForPair(p1, p2));
    return `M${pr(secondToLastPlus50pct)} C${pr(secondToLastPlus70pct)} ${pr(
        firstPlus30pct,
    )} ${pr(firstPlus50pct)}${
        pathSegments.length > 0 ? " " : ""
    }${pathSegments.join(" ")}`;
};
export const renderRegularPath = (points: ScreenCoordinates[]): string => {
    const pointPairs = windows(2, points);
    const [firstPlus30pct, firstPlus50pct] = interpolatePair(
        pointPairs[0],
        [0.3, 0.5],
    );
    const [secondToLastPlus70pct] = interpolatePair(
        pointPairs[pointPairs.length - 1],
        [0.7],
    );
    const pathSegments = pointPairs
        .slice(1)
        .map(([p1, p2]) => pathForPair(p1, p2));
    return `M${pr(firstPlus30pct)} C${pr(firstPlus50pct)} ${pr(
        firstPlus30pct,
    )} ${pr(firstPlus50pct)}${
        pathSegments.length > 0 ? " " : ""
    }${pathSegments.join(" ")} L${pr(secondToLastPlus70pct)}`;
};
const renderPath = (points: Coordinates[]): string => {
    const screenPointPairs = fillIntermediatePoints(points)
        .map(asMapCoordinates)
        .map((c) => mapToScreen(c));
    const beginning = points[0],
        end = points[points.length - 1];
    if (distanceSquared(beginning, end) < epsilon) {
        return renderClosedLoopPath(closeLoop(screenPointPairs));
    } else {
        return renderRegularPath(screenPointPairs);
    }
};
export const renderLine = (
    pathAttrByTag: { [tag: string]: Attributes | undefined },
    offsetByLevel: Map<number, MapCoordinates>,
    l: LineWithID,
): string =>
    `<path id="${l.id}" ${renderAttributes(
        pathAttrByTag[l.tag] ?? {},
    )} d="${renderPath(offsetLine(offsetByLevel, l))}" />`;
const renderLines = (
    { line, pathAttrByTag }: TextMapperMap,
    offsetByLevel: Map<number, MapCoordinates>,
): string =>
    `<g id="lines">${line
        .map((l): string => renderLine(pathAttrByTag, offsetByLevel, l))
        .join("\n")}</g>`;

const renderThings = (
    { region, baseAttrByTag }: TextMapperMap,
    offsetByLevel: Map<number, MapCoordinates>,
): string =>
    `<g id="things">${region
        // Keep only those tags that do not have attributes defined:
        .map(
            (r): Region => ({
                ...r,
                tags: r.tags.filter((t) => baseAttrByTag[t] === undefined),
            }),
        )
        // Keep only those regions that not have attributes defined for (some of) their tags:
        .filter(({ tags }) => tags.length > 0)
        .map((r) => renderRegionAsThing(offsetByLevel, r))
        .join("\n")}</g>`;

const renderCoordinatesText = ({ region, textAttr }: TextMapperMap): string =>
    `<g id="coordinates">${region
        .map(({ coordinates }) => {
            const { x: mx, y: my } = coordinates;
            const { x: sx, y: sy } = add(
                mapToScreen(coordinates),
                asScreenCoordinates({
                    x: 0,
                    y: -dy * 0.4,
                }),
            );
            return `<text text-anchor="middle" x="${sx.toFixed(
                1,
            )}" y="${sy.toFixed(1)}" ${renderAttributes(
                textAttr,
            )}>${renderCoordinateNumber(mx)}.${renderCoordinateNumber(
                my,
            )}</text>`;
        })
        .join("\n")}</g>`;

const renderRegionID = ({ coordinates: { x, y }, level }: Region): string =>
    x < 100 && y < 100 && level < 100
        ? `${renderCoordinateNumber(x)}${renderCoordinateNumber(y)}${
              level !== 0 ? renderCoordinateNumber(level) : ""
          }`
        : `${renderCoordinateNumber(x)}.${renderCoordinateNumber(y)}.${
              level !== 0 ? renderCoordinateNumber(level) : ""
          }`;
const renderRegion = (
    r: Region,
    offsetByLevel: Map<number, MapCoordinates>,
    defaultBaseAttributes: Attributes,
): string => {
    const screenCoordinates = mapToScreen(offsetRegion(offsetByLevel, r));
    return `<polygon id="hex${renderRegionID(r)}" ${renderAttributes(
        defaultBaseAttributes,
    )} points="${hexPolygonPoints
        .map((p) => add(screenCoordinates, p))
        .map(pr)
        .join(" ")}" />`;
};
const renderRegions = (
    { region, baseAttrByTag }: TextMapperMap,
    offsetByLevel: Map<number, MapCoordinates>,
): string => {
    const defaultBaseAttributes =
        baseAttrByTag["default"] !== undefined
            ? baseAttrByTag["default"]
            : {
                  fill: "none",
              };
    return `<g id="regions">${region
        .map((r) => renderRegion(r, offsetByLevel, defaultBaseAttributes))
        .join("\n")}</g>`;
};

const labelSide = ({
    label: { side },
    points,
}: LineWithLabel): "left" | "right" => {
    if (side !== undefined) {
        return side;
    }
    if (
        (points.length > 0 && points[1].x < points[0].x) ||
        (points.length > 1 && points[2].x < points[0].x)
    ) {
        return "right";
    }
    // default is "left": https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/side
    return "left";
};
const renderLineLabel = (
    baseURL: string | undefined,
    labelAttributes: Attributes,
    glowAttributes: Attributes,
    line: LineWithIDAndLabel,
): string => {
    const {
        id,
        label: { text, startOffsetPercentage },
    } = line;
    const textAttributes = renderAttributes(labelAttributes);
    const glowTextAttributes = renderAttributes({
        ...labelAttributes,
        ...glowAttributes,
    });
    const url = baseURL?.replace("%s", encodeURIComponent(text));
    // default is "0": https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/startOffset
    const startOffset =
        startOffsetPercentage !== undefined ? `${startOffsetPercentage}%` : "0";
    const pathAttributes = renderAttributes({
        side: labelSide(line),
        startOffset: startOffset,
    });
    return `<g>
${
    glowTextAttributes.length > 0
        ? `<text ${glowTextAttributes}><textPath ${pathAttributes} href="#${id}">${text}</textPath></text>`
        : ""
}
${url ? `<a xlink:href="${url}">` : ""}
<text ${textAttributes}><textPath ${pathAttributes} href="#${id}">${text}</textPath></text>
${url ? "</a>" : ""}
</g>`;
};
const renderLineLabels = ({
    line,
    baseURL,
    labelAttr,
    glowAttr,
}: TextMapperMap): string =>
    `<g id="line_labels">${line
        .filter(isLineWithIDAndLabel)
        .map((l) => renderLineLabel(baseURL, labelAttr, glowAttr, l))
        .join("\n")}</g>`;

export const renderRegionLabel = (
    baseURL: string | undefined,
    labelAttributes: Attributes,
    glowAttributes: Attributes,
    { x, y }: ScreenCoordinates,
    { text, fontSizePts }: RegionLabel,
): string => {
    const extendedLabelAttributes = {
        ...labelAttributes,
        ...(fontSizePts ? { "font-size": `${fontSizePts}pt` } : {}),
    };
    const textAttributes = renderAttributes(extendedLabelAttributes);
    const glowTextAttributes = renderAttributes({
        ...extendedLabelAttributes,
        ...glowAttributes,
    });
    const url = baseURL?.replace("%s", encodeURIComponent(text));
    return `<g>${
        glowTextAttributes.length > 0
            ? `<text text-anchor="middle" x="${x.toFixed(1)}" y="${y.toFixed(
                  1,
              )}" ${glowTextAttributes}>${text}</text>`
            : ""
    }${
        url ? `<a xlink:href="${url}">` : ""
    }<text text-anchor="middle" x="${x.toFixed(1)}" y="${y.toFixed(
        1,
    )}" ${textAttributes}>${text}</text>${url ? "</a>" : ""}</g>`;
};
const renderRegionLabels = ({
    region,
    baseURL,
    labelAttr,
    glowAttr,
}: TextMapperMap): string =>
    `<g id="labels">${region
        .filter(({ labels }) => labels.length > 0)
        .flatMap(({ coordinates, labels }) => {
            const screenCoordinates = add(
                mapToScreen(coordinates),
                asScreenCoordinates({ x: 0, y: dy * 0.4 }),
            );
            return labels.map((label) =>
                renderRegionLabel(
                    baseURL,
                    labelAttr,
                    glowAttr,
                    screenCoordinates,
                    label,
                ),
            );
        })
        .join("\n")}</g>`;

const renderOther = ({ other }: TextMapperMap): string =>
    other.map((o) => o.text).join("\n");

const groupByTagAndMergeObjects = <
    T extends { tag: string; attributes: Attributes },
>(
    values: T[] | undefined,
): { [tag: string]: Attributes | undefined } =>
    Object.fromEntries(
        Object.entries(
            objectGroupByKey(values ?? [], "tag") as { [tag: string]: T[] },
        ).map(([k, t]) => [k, mergeObjects(t)?.attributes]),
    );

const prepareMap = ({
    region,
    line,
    path,
    svgRawDefinition,
    textAttributes,
    labelAttributes,
    glowAttributes,
    baseAttributes,
    pathAttributes,
    baseURL,
    license,
    other,
}: GroupByKey<TextMapperValue, "type">): TextMapperMap => {
    return {
        region: region ?? [],
        line: (line ?? []).map(
            (l, i): LineWithID => ({ ...l, id: `line${i}` }),
        ),
        svgRawDefinition: svgRawDefinition ?? [],
        textAttr: mergeObjects(textAttributes ?? [])?.attributes ?? {},
        labelAttr: mergeObjects(labelAttributes ?? [])?.attributes ?? {},
        glowAttr: mergeObjects(glowAttributes ?? [])?.attributes ?? {},
        baseAttrByTag: groupByTagAndMergeObjects(baseAttributes),
        pathAttrByTag: groupByTagAndMergeObjects(pathAttributes),
        baseURL: mergeObjects(baseURL ?? [])?.url,
        license: mergeObjects(license ?? [])?.text ?? "",
        other: other ?? [],
        path: path ?? [],
    };
};

export type RenderOptions = {
    xmlDeclaration: boolean;
    svgAttributes: Attributes;
};
const defaultOptions: RenderOptions = {
    xmlDeclaration: true,
    svgAttributes: {},
};
export const render = (
    xs: TextMapperValue[],
    { xmlDeclaration, svgAttributes }: RenderOptions = defaultOptions,
): string => {
    const m = prepareMap(objectGroupByKey(xs, "type"));
    const { region, license } = m;
    const offsetByLevel = regionOffsetByLevel(region);
    return `${
        xmlDeclaration
            ? `<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n`
            : ""
    }<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" ${renderViewBox(
        region,
    )} ${renderAttributes(svgAttributes)}>
${renderDefs(m)}
${renderBackgrounds(m, offsetByLevel)}
${renderLines(m, offsetByLevel)}
${renderThings(m, offsetByLevel)}
${renderCoordinatesText(m)}
${renderRegions(m, offsetByLevel)}
${renderLineLabels(m)}
${renderRegionLabels(m)}
${license}
${renderOther(m)}
</svg>`;
}; // FIXME: implement other_text (and maybe also other_info?)

export const renderFile = (xs: TextMapperValue[], fileName: string): void =>
    writeFileSync(fileName, render(xs), "utf-8");
