// SPDX-License-Identifier: AGPL-3.0-or-later

import {
    coroutine,
    either,
    lookAhead,
    Parser,
    possibly,
    sequenceOf,
} from "arcsecond";

// arcsecond's built-in `sepBy1` fails if it can parse `sep` but not `val`,
// which happens e.g. in `sequenceOf([possibly(sepBy1(sep,...)),sep,...])`
// and causes `possibly` to never match anything.
// cf. https://github.com/francisrstokes/arcsecond/issues/111
export const sepBy1 = <S, T>(sep: Parser<S>, val: Parser<T>): Parser<T[]> =>
    coroutine((run) => {
        const results: T[] = [run(val)];

        const next = sequenceOf([sep, val]);
        while (!run(either(lookAhead(next))).isError) {
            const [_, value] = run(next);
            results.push(value);
        }

        return results;
    });
export const sepBy2 = <T>(sep: Parser<string>, val: Parser<T>): Parser<T[]> =>
    coroutine((run) => {
        const results: T[] = [run(val)];

        const next = sequenceOf([sep, val]);
        const [_, value] = run(next);
        results.push(value);

        while (!run(either(lookAhead(next))).isError) {
            const [_, value] = run(next);
            results.push(value);
        }

        return results;
    });

// beware that `sepByTrailing` will consume trailing `sep`, so never
// use it in situations resembling `sequenceOf([sepBy1(sep,...),sep,...])`.
export const sepByTrailing = <T>(
    sep: Parser<string>,
    val: Parser<T>,
): Parser<T[]> =>
    coroutine((run) => {
        const results: T[] = [];

        for (;;) {
            const { isError, value } = run(either(val));
            if (isError) break;

            results.push(value);

            if (run(either(sep)).isError) break;
        }

        return results;
    });

// arcsecond's built-in `possibly` returns `T | null`, which is not compatible
// with optional properties (e.g. `{x?:string}`).
export const possiblyUndefined = <T, E, D>(
    parser: Parser<T, E, D>,
): Parser<T | undefined, E, D> => possibly(parser).map((x) => x ?? undefined);
