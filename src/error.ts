// SPDX-License-Identifier: AGPL-3.0-or-later

import { isNativeError } from "util/types";

export const isErrnoException = (err: unknown): err is ErrnoException =>
    isNativeError(err) &&
    Object.hasOwn(err, "errno") &&
    Object.hasOwn(err, "code") &&
    Object.hasOwn(err, "path") &&
    Object.hasOwn(err, "syscall");
