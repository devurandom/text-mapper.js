// SPDX-License-Identifier: AGPL-3.0-or-later

import { expect, test, describe } from "bun:test";
import { fillIntermediatePoints, nextPoint } from "./path";

describe("nextPoint", () => {
    test("0005-0506 trail", () => {
        expect(nextPoint({ x: 0, y: 6 }, { x: 5, y: 6 })).toEqual({
            x: 1,
            y: 6,
        });
    });
    test("0506-0005 trail", () => {
        expect(nextPoint({ x: 5, y: 6 }, { x: 0, y: 5 })).toEqual({
            x: 4,
            y: 5,
        });
    });
    test("0205-0203-0103-0105-0205 trail", () => {
        expect(nextPoint({ x: 2, y: 5 }, { x: 2, y: 3 })).toEqual({
            x: 2,
            y: 4,
        });
    });
    test("0707-0607 communication", () => {
        expect(nextPoint({ x: 7, y: 7 }, { x: 6, y: 7 })).toEqual({
            x: 6,
            y: 7,
        });
    });
});

describe("fillIntermediatePoints", () => {
    test("0005-0506 trail", () => {
        expect(
            fillIntermediatePoints([
                { x: 0, y: 5 },
                { x: 5, y: 6 },
            ]),
        ).toEqual([
            { x: 0, y: 5 },
            { x: 1, y: 6 },
            { x: 2, y: 6 },
            { x: 3, y: 6 },
            { x: 4, y: 6 },
            { x: 5, y: 6 },
        ]);
    });
    test("0205-0203-0103-0105-0205 trail", () => {
        expect(
            fillIntermediatePoints([
                { x: 2, y: 5 },
                { x: 2, y: 3 },
                { x: 1, y: 3 },
                { x: 1, y: 5 },
                { x: 2, y: 5 },
            ]),
        ).toEqual([
            { x: 2, y: 5 },
            { x: 2, y: 4 },
            { x: 2, y: 3 },
            { x: 1, y: 3 },
            { x: 1, y: 4 },
            { x: 1, y: 5 },
            { x: 2, y: 5 },
        ]);
    });
    test("0707-0607 communication", () => {
        expect(
            fillIntermediatePoints([
                { x: 7, y: 7 },
                { x: 6, y: 7 },
            ]),
        ).toEqual([
            { x: 7, y: 7 },
            //{ x: 6, y: 6 }, // FIXME: Remove this
            { x: 6, y: 7 },
        ]);
    });
    test("0804-0605 communication", () => {
        expect(
            fillIntermediatePoints([
                { x: 8, y: 4 },
                { x: 6, y: 5 },
            ]),
        ).toEqual([
            { x: 8, y: 4 },
            //{ x: 7, y: 4 }, // FIXME: Remove this
            { x: 7, y: 5 },
            //{ x: 6, y: 4 }, // FIXME: Remove this
            { x: 6, y: 5 },
        ]);
    });
});
