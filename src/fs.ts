// SPDX-License-Identifier: AGPL-3.0-or-later

import { readFileSync } from "fs";
import { isErrnoException } from "./error";

export const maybeReadFileSync = (
    path: PathOrFileDescriptor,
    options: BufferEncoding,
): string | undefined => {
    try {
        return readFileSync(path, options);
    } catch (err) {
        if (isErrnoException(err) && err.code === "ENOENT") {
            return undefined;
        }
        throw err;
    }
};
