// SPDX-License-Identifier: AGPL-3.0-or-later

import { range, windows } from "./collection";
import { add, Coordinates, distanceSquared, epsilon } from "./coordinates";

/*
 Brute forcing the "next" step by trying all the neighbors. The
 connection data to connect to neighboring hexes.

 Example Map             Index for the `directions` array

      0201                      2
  0102    0302               1     3
      0202    0402
  0103    0303               6     4
      0203    0403              5
  0104    0304

 Note that the arithmetic changes when x is odd.
*/
const directions: Coordinates[][] = [
    [
        // use this table when x is even:
        { x: -1, y: 0 },
        { x: 0, y: -1 },
        { x: +1, y: 0 },
        { x: +1, y: +1 },
        { x: 0, y: +1 },
        { x: -1, y: +1 },
    ],
    [
        // use this table when x is odd:
        { x: -1, y: -1 },
        { x: 0, y: -1 },
        { x: +1, y: -1 },
        { x: +1, y: 0 },
        { x: 0, y: +1 },
        { x: -1, y: 0 },
    ],
];
export const nextPoint = (from: Coordinates, to: Coordinates): Coordinates => {
    const lookupTable = from.x % 2;
    const { closestPoint } = range(6).reduce(
        ({ closestDSq, closestPoint }, n) => {
            const candidate = add(from, directions[lookupTable][n]);
            const dSq = distanceSquared(candidate, to);
            if (dSq < closestDSq) {
                return { closestDSq: dSq, closestPoint: candidate };
            } else {
                return { closestDSq, closestPoint };
            }
        },
        { closestDSq: Number.MAX_VALUE, closestPoint: {} as Coordinates },
    );
    return closestPoint;
};
export const intermediatePoints = (
    from: Coordinates,
    to: Coordinates,
    intermediate: Coordinates[] = [],
): Coordinates[] => {
    const next = nextPoint(from, to);
    if (distanceSquared(next, to) > epsilon) {
        return intermediatePoints(next, to, [...intermediate, from]);
    } else {
        return [...intermediate, from];
    }
};
export const fillIntermediatePoints = (
    points: Coordinates[],
): Coordinates[] => {
    const end = points[points.length - 1];
    return [
        ...windows(2, points).flatMap(([previous, next]): Coordinates[] =>
            intermediatePoints(previous, next),
        ),
        end,
    ];
};
