// SPDX-License-Identifier: AGPL-3.0-or-later

import { readFileSync } from "fs";
import { join } from "path";
import { char, choice, lookAhead, regex, sequenceOf, str } from "arcsecond";
import { possiblyUndefined, sepBy1, sepBy2, sepByTrailing } from "./xarcsecond";
import { objectGroupByKey, some } from "./collection";
import { maybeReadFileSync } from "./fs";
import { asMapCoordinates, MapCoordinates } from "./coordinates";

export type Comment = {
    type: "comment";
    value: string;
};
export type RegionLabel = {
    type: "regionLabel";
    text: string;
    fontSizePts?: number;
    svgTransformations?: string[];
};
export type Region = {
    type: "region";
    level: number;
    coordinates: MapCoordinates;
    tags: string[];
    labels: RegionLabel[];
};
export type LineLabel = {
    text: string;
    side?: "left" | "right";
    startOffsetPercentage?: number;
};
export type Line = {
    type: "line";
    level: number;
    points: MapCoordinates[];
    tag: string;
    label?: LineLabel;
};
export type SVGRawDefinition = {
    type: "svgRawDefinition";
    value: string;
};
export type Attributes = { [k: string]: string };
export type SVGBaseAttributes = {
    type: "baseAttributes";
    tag: string;
    attributes: Attributes;
};
export type SVGPathAttributes = {
    type: "pathAttributes";
    tag: string;
    attributes: Attributes;
};
export type SVGPath = {
    type: "path";
    tag: string;
    path: string;
};
export type TextAttributes = {
    type: "textAttributes";
    attributes: Attributes;
};
export type GlowAttributes = {
    type: "glowAttributes";
    attributes: Attributes;
};
export type LabelAttributes = {
    type: "labelAttributes";
    attributes: Attributes;
};
export type License = {
    type: "license";
    text: string;
};
export type Other = {
    type: "other";
    text: string;
};
export type BaseURL = {
    type: "baseURL";
    url: string;
};
export type Include = {
    type: "include";
    ref: string;
    path?: string;
};

const newline = regex(/^(\r\n|\n)/);
const emptyLine = lookAhead(newline).map(
    (): Comment => ({ type: "comment", value: "" }),
);
const comment = regex(/^[ \t]*#[^\n]*/).map(
    (input): Comment => ({
        type: "comment",
        value: input,
    }),
);
const space = regex(/^[ \t]+/);
const optSpace = possiblyUndefined(space);
const word = regex(/^[a-zA-Z0-9_-]+/);
const integer = regex(/^\d+/).map(Number.parseInt);
const quotedString = regex(/^"[^"]+"/).map((input) =>
    input.substring(1, input.length - 1),
);

type Coordinates = { x: number; y: number; z: number };
const coordinatesFromXYZ = (x: string, y: string, z: string): Coordinates => {
    const zAsInt = Number.parseInt(z);
    return {
        x: Number.parseInt(x),
        y: Number.parseInt(y),
        z: !Number.isNaN(zAsInt) ? zAsInt : 0,
    };
};

type Matcher = { [Symbol.match](string: string): RegExpMatchArray | null };
const maybeMatch = (s: string, m: Matcher): RegExpMatchArray | [] =>
    s.match(m) ?? [];

const point = choice([
    regex(/^(-?\d\d)(-?\d\d)(\d\d)?/).map((input) => {
        const [_, x, y, z] = maybeMatch(input, /^(-?\d\d)(-?\d\d)(\d\d)?/);
        return coordinatesFromXYZ(x, y, z);
    }),
    regex(/^-?\d\d+\.-?\d\d+(?:\.\d\d+)?/).map((input) => {
        const [_, x, y, z] =
            input.match(/^(-?\d\d+)\.(-?\d\d+)(?:\.(\d\d+))?/) ?? [];
        return coordinatesFromXYZ(x, y, z);
    }),
]);
const svgTransformation = regex(/^\w+\([^)]+\)/);

// match: /["“]([^"”]+)["”]\s*(\d+)?((?:\s*[a-z]+\([^\)]+\))*)/
const regionLabel = sequenceOf([
    quotedString,
    possiblyUndefined(
        sequenceOf([space, integer]).map(([_, fontSize]) => fontSize),
    ),
    possiblyUndefined(
        sequenceOf([space, sepBy1(optSpace, svgTransformation)]).map(
            ([_, svgTransformations]) => svgTransformations,
        ),
    ),
]).map(
    ([text, fontSize, svgTransformations]): RegionLabel => ({
        type: "regionLabel",
        text,
        fontSizePts: fontSize,
        svgTransformations,
    }),
);

type RegionTag = {
    type: "regionTag";
    value: string;
};
const regionTag = word.map((v): RegionTag => ({ type: "regionTag", value: v }));

// match: /^(-?\d\d)(-?\d\d)(\d\d)?\s+(.*)/
//    or: /^(-?\d\d+)\.(-?\d\d+)(?:\.(\d\d+))?\s+(.*)/
const region = sequenceOf([
    point,
    space,
    possiblyUndefined(
        sepBy1(optSpace, choice([regionLabel, regionTag])).map((values) =>
            objectGroupByKey(values, "type"),
        ),
    ),
]).map(
    ([{ x, y, z }, _, opt]): Region => ({
        type: "region",
        level: z,
        coordinates: asMapCoordinates({ x, y }),
        tags: opt?.regionTag?.map(({ value }) => value) ?? [],
        labels: opt?.regionLabel ?? [],
    }),
);

const side = choice([
    str("left").map((input) => input as "left"),
    str("right").map((input) => input as "right"),
]);
const percentage = regex(/^\d+%/).map((input) =>
    Number.parseInt(input.substring(0, input.length - 1)),
);
const lineLabel = sequenceOf([
    quotedString,
    possiblyUndefined(sequenceOf([optSpace, side]).map(([_, side]) => side)),
    possiblyUndefined(
        sequenceOf([optSpace, percentage]).map(([_, percentage]) => percentage),
    ),
]).map(
    ([text, side, startOffsetPercentage]): LineLabel => ({
        text,
        side,
        startOffsetPercentage,
    }),
);

const pointList = sepBy2(char("-"), point);
const validatePointList = (coordinates: Coordinates[]): Coordinates[] => {
    const zs = new Set(coordinates.map(({ z }) => z));
    if (zs.size !== 1) {
        throw new Error(
            `points in line must have same Z coordinate, found '${Array.from(
                zs,
            ).join(", ")}'`,
        );
    }
    return coordinates;
};
// match: /^(-?\d\d-?\d\d(?:\d\d)?(?:--?\d\d-?\d\d(?:\d\d)?)+)\s+(\S+)\s*(?:["“](.+)["”])?\s*(left|right)?\s*(\d+%)?/
//    or: /^(-?\d\d+\.-?\d\d+(?:\.\d\d+)?(?:--?\d\d+\.-?\d\d+(?:\.\d\d+)?)+)\s+(\S+)\s*(?:["“](.+)["”])?\s*(left|right)?\s*(\d+%)?/
const line = sequenceOf([
    pointList.map(validatePointList),
    space,
    word,
    possiblyUndefined(
        sequenceOf([optSpace, lineLabel]).map(([_, lineLabel]) => lineLabel),
    ),
]).map(
    ([points, _, tag, lineLabel]): Line => ({
        type: "line",
        level: points[0].z,
        points: points.map(asMapCoordinates),
        tag,
        label: lineLabel,
    }),
);

const rawSVG = regex(/^<[^\n]*/);
const semiRawSVG = sequenceOf([
    word,
    space,
    choice([str("xml"), str("lib")]),
    space,
    rawSVG,
]).map(([id, _1, _2, _3, content]): string => `<g id="${id}">${content}</g>`);

// match: /^(\S+)\s+lib\s+(.*)/
//    or: /^(\S+)\s+xml\s+(.*)/
//    or: /^(<.*>)/
const svgRawDefinition = choice([semiRawSVG, rawSVG]).map(
    (value): SVGRawDefinition => ({
        type: "svgRawDefinition",
        value,
    }),
);

const attribute = sequenceOf([word, char("="), quotedString]).map(
    ([name, _, value]): [string, string] => [name, value],
);
const attributeList = sepBy1(optSpace, attribute).map((values) =>
    Object.fromEntries(values),
);

// match: /^(\S+)\s+attributes\s+(.*)/
const svgBaseAttributes = sequenceOf([
    word,
    space,
    str("attributes"),
    space,
    attributeList,
]).map(
    ([tag, _1, _2, _3, attributes]): SVGBaseAttributes => ({
        type: "baseAttributes",
        tag,
        attributes,
    }),
);

// match: /^(\S+)\s+path\s+attributes\s+(.*)/
const svgPathAttributes = sequenceOf([
    word,
    space,
    str("path attributes"),
    space,
    attributeList,
]).map(
    ([tag, _1, _2, _3, attributes]): SVGPathAttributes => ({
        type: "pathAttributes",
        tag,
        attributes,
    }),
);

// match: /^(\S+)\s+path\s+(.*)/
const svgPath = sequenceOf([
    word,
    space,
    str("path"),
    space,
    regex(/^[^\n]*/),
]).map(
    ([tag, _1, _2, _3, path]): SVGPath => ({
        type: "path",
        tag,
        path,
    }),
);

// match: /^text\s+(.*)/
const textAttributes = sequenceOf([str("text"), space, attributeList]).map(
    ([_1, _2, attributes]): TextAttributes => ({
        type: "textAttributes",
        attributes,
    }),
);

// match: /^glow\s+(.*)/
const glowAttributes = sequenceOf([str("glow"), space, attributeList]).map(
    ([_1, _2, attributes]): GlowAttributes => ({
        type: "glowAttributes",
        attributes,
    }),
);

// match: /^label\s+(.*)/
const labelAttributes = sequenceOf([str("label"), space, attributeList]).map(
    ([_1, _2, attributes]): LabelAttributes => ({
        type: "labelAttributes",
        attributes,
    }),
);

// match: /^license\s+(.*)/
const license = sequenceOf([str("license"), space, regex(/^[^\n]*/)]).map(
    ([_1, _2, text]): License => ({
        type: "license",
        text,
    }),
);

// match: /^other\s+(.*)/
const other = sequenceOf([str("other"), space, regex(/^[^\n]*/)]).map(
    ([_1, _2, text]): Other => ({
        type: "other",
        text,
    }),
);

// match: /^url\s+(.*)/
const url = sequenceOf([str("url"), space, regex(/^[^\n]*/)]).map(
    ([_1, _2, url]): BaseURL => ({
        type: "baseURL",
        url,
    }),
);

// match: /^include\s+(.*)/
const include = sequenceOf([str("include"), space, regex(/^[^\n]*/)]).map(
    ([_1, _2, ref]): Include => ({
        type: "include",
        ref,
    }),
);

const statement = choice([
    emptyLine,
    comment,
    region,
    line,
    svgRawDefinition,
    svgBaseAttributes,
    svgPathAttributes,
    svgPath,
    textAttributes,
    glowAttributes,
    labelAttributes,
    license,
    other,
    url,
    include,
]);

const textMapperFile = sepByTrailing(
    newline, // FIXME: sepByTrailing does not return the last value in the list if it is not followed by `sep`. // FIXME: Still true?
    sequenceOf([statement, optSpace]).map(([v, _]) => v),
);

// `textMapperFile.map((xs)=>{})` takes a function as first argument that
// as first argument receives the value returned by parsing the file:
export type TextMapperValue = Parameters<
    Parameters<typeof textMapperFile.map>[0]
>[0][number];

const byteLength = (s: string): number =>
    new TextEncoder().encode(s).byteLength;

export type FileFound = { path: string; data: string };
export const parse = (
    data: string,
    read: (fileName: string) => FileFound,
): TextMapperValue[] => {
    const result = textMapperFile.run(data);
    if (result.isError) {
        throw new Error(`failed to parse: ${result.error}`);
    }

    if (result.index !== byteLength(data)) {
        throw {
            message: `parser did not consume all input: ${result.index} parsed, ${data.length} available`,
            value: result.result,
        };
    }

    return result.result.flatMap((v): TextMapperValue[] => {
        if (v.type === "include") {
            const { path, data } = read(v.ref);
            return [{ ...v, path }, ...parse(data, read)];
        } else {
            return [v];
        }
    });
};

export const readFile =
    (searchPath: string[]) =>
    (ref: string): FileFound => {
        const result = some((p) => {
            const path = join(p, ref);
            const data = maybeReadFileSync(path, "utf-8");
            if (data !== undefined) {
                return { path, data };
            } else {
                return undefined;
            }
        }, searchPath);
        if (result === undefined) {
            throw Error(`no such file or directory: ${ref}`);
        }
        return result;
    };

export const parseFile = (
    fileName: string,
    searchPath: string[],
): TextMapperValue[] => {
    return parse(readFileSync(fileName, "utf-8"), readFile(searchPath));
};
